package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.IDestination;
import fr.umontpellier.iut.IJeu;
import fr.umontpellier.iut.IJoueur;
import fr.umontpellier.iut.rails.Destination;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * Cette classe correspond à la fenêtre principale de l'application.
 *
 * Elle est initialisée avec une référence sur la partie en cours (Jeu).
 *
 * On y définit les bindings sur les éléments internes qui peuvent changer
 * (le joueur courant, les 5 cartes Wagons visibles, les destinations lors de l'étape d'initialisation de la partie, ...)
 * ainsi que les listeners à exécuter lorsque ces éléments changent
 */
public class VueDuJeu extends BorderPane {

    private IJeu jeu;
    private VuePlateau vuePlateau;
    private VueJoueurCourant vueJoueurCourant;

    private VBox vueJeu;
    private Button passer;
    private BorderPane vueBottom;
    private HBox listeDestinations;
    private Label labelInstruction;

    public VueDuJeu(IJeu jeu) {
        this.jeu = jeu;
        vueJeu = new VBox();
        vuePlateau = new VuePlateau();
        vueJoueurCourant = new VueJoueurCourant();
        vueBottom = new BorderPane();
        listeDestinations = new HBox();
        passer = new Button("Passer");
        vueJeu.getChildren().addAll(vueJoueurCourant, passer);
        labelInstruction = new Label(getJeu().instructionProperty().getValue());
        this.setTop(labelInstruction);
        this.setRight(vueJeu);
        this.setCenter(vuePlateau);
        this.setBottom(vueBottom);
        vueBottom.setCenter(listeDestinations);
    }

    public IJeu getJeu() {
        return jeu;
    }

    public void creerBindings() {
        passer.setOnAction(action->jeu.passerAEteChoisi());
        jeu.joueurCourantProperty().addListener(joueurCourantListener);
        jeu.destinationsInitialesProperty().addListener(destinationsSontPiocheesListener);
        vueJoueurCourant.creerBindings();
        vuePlateau.creerBindings();
        labelInstruction.textProperty().bind(getJeu().instructionProperty());
    }

    private final ListChangeListener<IDestination> destinationsSontPiocheesListener = new ListChangeListener<IDestination>() {
        @Override
        public void onChanged(Change<? extends IDestination> change) {
            Platform.runLater(() -> {
               // switchToVueCartes(listeDestinations);
                while (change.next()) {
                    if (change.wasAdded())
                        for (IDestination d : change.getAddedSubList()) {
                            listeDestinations.getChildren().add(new VueDestination(d));
                            System.out.println(d.getNom());
                        }
                    if (change.wasRemoved())
                        for (IDestination d : change.getRemoved())
                            listeDestinations.getChildren().remove(trouveDestination(d));
                }
            });
        }
    };

    private VueDestination trouveDestination(IDestination destination) {
        for (Node n : listeDestinations.getChildren()) {
            VueDestination vueDestination = (VueDestination) n;
            if (vueDestination.getDestination().equals(destination))
                return vueDestination;
        }
        return null;
    }

    private final ChangeListener<IJoueur> joueurCourantListener = new ChangeListener<IJoueur>() {
        @Override
        public void changed(ObservableValue<? extends IJoueur> observableValue, IJoueur ancienJoueur, IJoueur nouveauJoueur) {
            Platform.runLater(() -> {
                listeDestinations.getChildren().clear();
            });
        }
    };

    public void switchToVueCartes(HBox nouvelleVueCartes) {
        vueBottom.setCenter(nouvelleVueCartes);
    }
}