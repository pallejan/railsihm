package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.ICouleurWagon;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

/**
 * Cette classe représente la vue d'une carte Wagon.
 *
 * On y définit le listener à exécuter lorsque cette carte a été choisie par l'utilisateur
 */
public class VueCarteWagon extends Button {

    private ICouleurWagon couleurWagon;
    private Image image;

    public VueCarteWagon(ICouleurWagon couleurWagon, Boolean carteJoueur) {

        this.couleurWagon = couleurWagon;
        image = new Image("images/cartesWagons/carte-wagon-" + getCouleurWagon().toString().toUpperCase(). replaceAll("[. !]+", "")+ ".png");
        ImageView imageView = new ImageView(image);
        imageView.setPreserveRatio(true);
        if (carteJoueur) {
            imageView.setFitWidth(55);
            imageView.setFitHeight(40);
        } else {
            imageView.setFitWidth(145);
            imageView.setFitHeight(80);
        }
        setPadding(new Insets(0,0,0,0));
        setGraphic(imageView);
    }

    public ICouleurWagon getCouleurWagon() {
        return couleurWagon;
    }

}
