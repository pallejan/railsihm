package fr.umontpellier.iut.vues;

import javafx.beans.binding.DoubleBinding;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;

import java.io.IOException;

/**
 * Cette classe présente les routes et les villes sur le plateau.
 *
 * On y définit le listener à exécuter lorsque qu'un élément du plateau a été choisi par l'utilisateur
 * ainsi que les bindings qui mettront ?à jour le plateau après la prise d'une route ou d'une ville par un joueur
 */
public class VuePlateau extends Pane {
    @FXML
    private Group villes;
    @FXML
    private Group routes;
    @FXML
    private ImageView imageView;

    public VuePlateau() {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/plateau.fxml"));
            loader.setRoot(this);
            loader.setController(this);
            loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void creerBindings() {
        bindRedimensionPlateau();
    }

    @FXML
    public void choixRouteOuVille() {
    }

    private void bindRedimensionPlateau() {
        bindRoutes();
        bindVilles();
        imageView.fitWidthProperty().bind(getScene().widthProperty().multiply(.8));
        imageView.fitHeightProperty().bind(getScene().heightProperty().multiply(.817317));
    }

    private void bindRectangle(Rectangle rect, double layoutX, double layoutY) {
        rect.widthProperty().bind(new DoubleBinding() {
            { super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());}
            @Override
            protected double computeValue() {
                return DonneesPlateau.largeurRectangle * imageView.getLayoutBounds().getWidth() / DonneesPlateau.largeurInitialePlateau;
            }
        });
        rect.heightProperty().bind(new DoubleBinding() {
            { super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());}
            @Override
            protected double computeValue() {
                return DonneesPlateau.hauteurRectangle * imageView.getLayoutBounds().getWidth()/ DonneesPlateau.largeurInitialePlateau;
            }
        });
        rect.layoutXProperty().bind(new DoubleBinding() {
            {
                super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());
            }
            @Override
            protected double computeValue() {
                return layoutX * imageView.getLayoutBounds().getWidth()/ DonneesPlateau.largeurInitialePlateau;
            }
        });
        rect.xProperty().bind(new DoubleBinding() {
            { super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());}
            @Override
            protected double computeValue() {
                return DonneesPlateau.xInitial * imageView.getLayoutBounds().getWidth() / DonneesPlateau.largeurInitialePlateau;
            }
        });
        rect.layoutYProperty().bind(new DoubleBinding() {
            {
                super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());
            }
            @Override
            protected double computeValue() {
                return layoutY * imageView.getLayoutBounds().getHeight()/ DonneesPlateau.hauteurInitialePlateau;
            }
        });
        rect.yProperty().bind(new DoubleBinding() {
            { super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());}
            @Override
            protected double computeValue() {
                return DonneesPlateau.yInitial * imageView.getLayoutBounds().getHeight()/ DonneesPlateau.hauteurInitialePlateau;
            }
        });
    }

    private void bindRoutes() {
        for (Node nRoute : routes.getChildren()) {
            Group gRoute = (Group) nRoute;
            int numRect = 0;
            for (Node nRect : gRoute.getChildren()) {
                Rectangle rect = (Rectangle) nRect;
                bindRectangle(rect, DonneesPlateau.getRoute(nRoute.getId()).get(numRect).getLayoutX(), DonneesPlateau.getRoute(nRoute.getId()).get(numRect).getLayoutY());
                numRect++;
            }
        }
    }

    private void bindVilles() {
        for (Node nVille : villes.getChildren()) {
            Circle ville = (Circle) nVille;
            bindVille(ville, DonneesPlateau.getVille(ville.getId()).getLayoutX(), DonneesPlateau.getVille(ville.getId()).getLayoutY());
        }
    }

    private void bindVille(Circle ville, double layoutX, double layoutY) {
        ville.layoutXProperty().bind(new DoubleBinding() {
            {
                super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());
            }
            @Override
            protected double computeValue() {
                return layoutX * imageView.getLayoutBounds().getWidth()/ DonneesPlateau.largeurInitialePlateau;
            }
        });
        ville.layoutYProperty().bind(new DoubleBinding() {
            {
                super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());
            }
            @Override
            protected double computeValue() {
                return layoutY * imageView.getLayoutBounds().getHeight()/ DonneesPlateau.hauteurInitialePlateau;
            }
        });
        ville.radiusProperty().bind(new DoubleBinding() {
            { super.bind(imageView.fitWidthProperty(), imageView.fitHeightProperty());}
            @Override
            protected double computeValue() {
                return DonneesPlateau.rayonInitial * imageView.getLayoutBounds().getWidth() / DonneesPlateau.largeurInitialePlateau;
            }
        });
    }

}
