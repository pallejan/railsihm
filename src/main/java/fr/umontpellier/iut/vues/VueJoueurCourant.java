package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.ICouleurWagon;
import fr.umontpellier.iut.IDestination;
import fr.umontpellier.iut.IJoueur;
import fr.umontpellier.iut.rails.CouleurWagon;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ListChangeListener;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 * Cette classe présente les éléments appartenant au joueur courant.
 *
 * On y définit les bindings sur le joueur courant, ainsi que le listener à exécuter lorsque ce joueur change
 */

public class VueJoueurCourant extends VBox {

    private Label nomJoueur;
    private HBox panneauCartesWagons;
    private VBox panneauDestinations;

    public VueJoueurCourant(){
        nomJoueur = new Label();
        panneauCartesWagons = new HBox();
        panneauDestinations = new VBox();
        this.getChildren().addAll(nomJoueur,panneauCartesWagons,panneauDestinations);
    }

    public void creerBindings() {
        ((VueDuJeu) getScene().getRoot()).getJeu().joueurCourantProperty().addListener(joueurChangeListener);
    }

    private final ChangeListener<IJoueur> joueurChangeListener = new ChangeListener<IJoueur>() {
        @Override
        public void changed(ObservableValue<? extends IJoueur> observableValue, IJoueur ancienJoueur, IJoueur nouveauJoueur) {
            Platform.runLater(() -> {
                nomJoueur.setText(nouveauJoueur.getNom());
                panneauCartesWagons.getChildren().clear();
                for (ICouleurWagon w : nouveauJoueur.getCartesWagon())
                    panneauCartesWagons.getChildren().add(new VueCarteWagon(w,true));
                panneauDestinations.getChildren().clear();
                for (IDestination d : nouveauJoueur.getDestinations())
                    panneauDestinations.getChildren().add(new Label(d.getNom()));
            });
        }
    };


}
