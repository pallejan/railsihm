package fr.umontpellier.iut.vues;

import fr.umontpellier.iut.IDestination;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

/**
 * Cette classe représente la vue d'une carte Destination.
 *
 * On y définit le listener à exécuter lorsque cette carte a été choisie par l'utilisateur
 */
public class VueDestination extends Button {

    private IDestination destination;

    public VueDestination(IDestination destination) {
        this.destination = destination;
        setText(destination.getNom());
        setPrefWidth(250);
        setPrefHeight(80);
        setDestinationChoisieListener();
    }

    public IDestination getDestination() {
        return destination;
    }

    public void setDestinationChoisieListener() {
        setOnMouseClicked(event -> ((VueDuJeu) getScene().getRoot()).getJeu().uneDestinationAEteChoisie(getText()));
    }
}
